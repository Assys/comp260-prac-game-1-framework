﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveArrow : MonoBehaviour {
	private BeeSpawner beeSpawner;




	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();


		
	}


	public float maxSpeed = 5.0f;
	Vector2 direction;
	public float acceleration = 1.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float brake = 15.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second
	public float destroyRadius = 1.0f;




	
	// Update is called once per frame
	void Update () {
		// scale the velocity by the frame duration


		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}
		// move the object

		// get the input values


	



		// the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal2");


		// turn the car
		transform.Rotate(0, 0, turn * turnSpeed * speed*2 * Time.deltaTime);



		float forwards = Input.GetAxis("Vertical2");
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			if (speed > 0) {
				speed = speed - (Mathf.Max(brake * Time.deltaTime, 0, Mathf.Abs(speed)));
			} else {
				speed = speed + (Mathf.Min(brake * Time.deltaTime, 0, Mathf.Abs(speed)));
			}
		}


		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);


		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);





		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}




	}

