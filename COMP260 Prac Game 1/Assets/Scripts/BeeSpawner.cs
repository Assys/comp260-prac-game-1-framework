﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
	public int nBees = 50;
	public BeeMove beePrefab;
	public PlayerMoveArrow player;
	public Rect spawnRect;
	public float xMin, yMin;
	public float width, height;

	public int minbeePeriod;
	public int maxbeePeriod;
	public int beePeriod;

	private int k;




	void Start() {
		//repeater...
		//starting in 0 seconds
		//a bee will be spawned every Random.Range
		beePeriod = Random.Range(minbeePeriod, maxbeePeriod);
		InvokeRepeating("spawnBees", 0, beePeriod);
	}


	//spawn method
	void spawnBees() {
		beePeriod = Random.Range(minbeePeriod, maxbeePeriod);
		// create bees
		for (int i = 0; i < nBees; i++) {
			// instantiate a bee
			BeeMove bee = Instantiate (beePrefab);


			// attach to this object in the hierarchy
			bee.transform.parent = transform;            
			// give the bee a name and number
			bee.gameObject.name = "Bee " + i;



			// move the bee to a random position within 
			// the spawn rectangle
			float x = spawnRect.xMin +
				Random.value * spawnRect.width;
			float y = spawnRect.yMin +
				Random.value * spawnRect.height;

			bee.transform.position = new Vector2 (x, y);
		}
	}




	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}






	void OnDrawGizmos() {
		// draw the spawning rectangle
		Gizmos.color = Color.green;
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMin));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMin));
	}


	void Update(){

	}

}